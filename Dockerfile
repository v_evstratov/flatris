FROM node

#make dir
RUN mkdir /app

#change dir for run build command below
WORKDIR /app

#coping json file for install dependencies
COPY package.json /app
#installing dependencies
RUN yarn install

#coping the remaining app's files
COPY . /app
#running build commands
RUN yarn test
RUN yarn build

#run command into container
#starting app
CMD yarn start

EXPOSE 3000
